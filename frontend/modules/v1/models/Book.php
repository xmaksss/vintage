<?php

namespace frontend\modules\v1\models;

use yii\helpers\Url;

/**
 * Class Book
 * @package frontend\modules\v1\models
 */
class Book extends \common\models\Book
{
    public function fields()
    {
        return [
            'id',
            'title',
            'image' => function ($model) {
                return Url::to($model->image ?: '/uploads/default.jpeg', true);
            },
            'author' => function ($model) {
                return $model->author->name;
            },
            'description'
        ];
    }
}