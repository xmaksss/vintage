<?php

namespace frontend\modules\v1\controllers;

use frontend\modules\v1\models\Book;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Class BookController
 * @package frontend\modules\v1\controllers
 */
class BookController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = Book::class;

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create']);

        return $actions;
    }

    /**
     * @return array
     */
    protected function verbs()
    {
        $actions = parent::verbs();

        $actions['update'] = ['POST', 'PUT'];

        return $actions;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $config = parent::behaviors();

        $config['contentNegotiator']['formats'] = [
            'application/json' => Response::FORMAT_JSON
        ];

        return $config;
    }
}