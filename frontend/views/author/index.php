<?php
/**
 * @var $authors \frontend\models\Author
 */

?>

<div class="author-index">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h3 class="text-center">Authors</h3>
            <ul class="list-group">
                <?php foreach ($authors as $author):?>
                    <?=\yii\helpers\Html::a($author->name . '<span class="badge">' . $author->booksCount .'</span>', ['author/' . $author->id . '/books'], ['class' => 'list-group-item'])?>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>
