<?php
/**
 * @var $book \frontend\models\Book
 */
use yii\helpers\Html;

?>

<div class="book-index">
    <div class="row">
        <div class="col-md-12">
            <?=\yii\helpers\Html::a('Back', ['author/' . $book->author_id . '/books/'], ['class' => 'btn btn-default' , 'style' => 'margin-bottom: 10px;'])?>
        </div>
        <div class="col-md-3">
            <?=\yii\helpers\Html::img([$book->image ?: 'uploads/default.jpeg'], [
                'width' => '100%'
            ])?>
        </div>
        <div class="col-md-6">
            <h3><?=Html::encode($book->title)?></h3>
            <p><b>Author: </b><?=Html::encode($book->author->name)?></p>
            <p><?=Html::encode($book->description)?></p>
        </div>
    </div>
</div>
