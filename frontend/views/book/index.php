<?php
/**
 * @var $author \frontend\models\Author
 */

?>

<div class="book-index">
    <div class="row">
        <div class="col-md-12">
            <?=\yii\helpers\Html::a('Back', ['author/'], ['class' => 'btn btn-default' , 'style' => 'margin-bottom: 10px;'])?>
        </div>
        <div class="col-md-6 col-md-offset-3">
            <h3 class="text-center">Books of author: <?=$author->name?></h3>
            <ul class="list-group">
                <?php foreach ($author->books as $book):?>
                    <?=\yii\helpers\Html::a($book->title, ['author/' . $author->id . '/books/' . $book->id], ['class' => 'list-group-item'])?>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>
