<?php

namespace frontend\controllers;

use frontend\models\Author;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays all authors.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(['author/index']);
    }
}
