<?php

namespace frontend\controllers;

use frontend\models\Author;
use yii\web\Controller;

/**
 * Class AuthorController
 * @package frontend\controllers
 */
class AuthorController extends Controller
{
    public function actionIndex()
    {
        $authors = Author::find()->all();

        return $this->render('index', [
            'authors' => $authors
        ]);
    }
}