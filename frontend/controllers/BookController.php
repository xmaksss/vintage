<?php

namespace frontend\controllers;

use frontend\models\Author;
use frontend\models\Book;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class BookController
 * @package frontend\controllers
 */
class BookController extends Controller
{
    /**
     * @param $author_id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($author_id)
    {
        $author = Author::findOne($author_id);

        if (!$author) {
            throw new NotFoundHttpException();
        }

        return $this->render('index', [
            'author' => $author
        ]);
    }

    public function actionView($author_id, $book_id)
    {
        $book = Book::findOne([
            'author_id' => $author_id,
            'id' => $book_id,
        ]);

        if (!$book) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'book' => $book
        ]);
    }
}