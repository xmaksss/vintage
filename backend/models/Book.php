<?php

namespace backend\models;

use yii\web\UploadedFile;

/**
 * Class Book
 * @package backend\models
 */
class Book extends \common\models\Book
{
    public $imageFile;

    /**
     * @param $imageFile UploadedFile
     * @return bool|string
     */
    public function upload($imageFile)
    {
        if ($this->validate()) {
            $file = 'uploads/' . self::_generateRandomName() . '.' . $imageFile->extension;
            $uploadPath = \Yii::getAlias('@frontend') . '/web/' . $file;

            $imageFile->saveAs($uploadPath);

            $this->image = $file;

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param int $length
     * @return string
     */
    private static function _generateRandomName($length = 24)
    {
        $name = '';

        $chars = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));

        for ($i = 0; $i < $length; $i++) {
            $name .= $chars[rand(0, count($chars) -1)];
        }

        return $name;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        $url = \Yii::$app->params['frontend_link'];
        return $this->image ? $url . '/' . $this->image : $url . '/uploads/default.jpeg';
    }
}