<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m180509_150643_insert_admin
 */
class m180509_150643_insert_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('user', [
            'username' => 'admin',
            'email' => 'admin@mail.com',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'auth_key' => Yii::$app->security->generateRandomString(),
            'status' => User::STATUS_ACTIVE,
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user', [
            'username' => 'admin',
        ]);
    }
}
