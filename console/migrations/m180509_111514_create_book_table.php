<?php

use yii\db\Migration;

/**
 * Handles the creation of table `book`.
 */
class m180509_111514_create_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('book', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'image' => $this->string(),
            'description' => $this->text()
        ]);

        $this->createIndex(
            'idx-book-author_id',
            'book',
            'author_id'
        );

        $this->addForeignKey(
            'fk-book-author_id',
            'book',
            'author_id',
            'author',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-book-author_id',
            'book'
        );

        $this->dropIndex(
            'idx-book-author_id',
            'book'
        );

        $this->dropTable('book');
    }
}
